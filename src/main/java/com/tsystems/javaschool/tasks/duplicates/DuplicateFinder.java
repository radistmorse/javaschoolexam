package com.tsystems.javaschool.tasks.duplicates;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || targetFile == null || !sourceFile.exists())
            throw new IllegalArgumentException();
        
        Map<String, Integer> strings = new TreeMap<>();
        
        try (Stream<String> stream = Files.lines(sourceFile.toPath())) {
            stream.forEach(line -> {
                if (strings.containsKey(line))
                    strings.replace(line, strings.get(line) + 1);
                else
                    strings.put(line, 1);
            });
            
        } catch (IOException e) {
                return false;
        } 
        
        try (BufferedWriter writer = Files.newBufferedWriter(targetFile.toPath(), StandardOpenOption.CREATE, StandardOpenOption.APPEND)) {
            for(Map.Entry<String,Integer> entry : strings.entrySet())
                writer.write(entry.getKey()+"["+entry.getValue().toString()+"]\n");
        } catch (IOException e) {
                return false;
        } 
        return true;
    }


}
