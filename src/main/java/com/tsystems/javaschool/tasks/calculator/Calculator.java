package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    private enum Operator
    {
        ADD(1) {
            @Override
            public double apply (double o1, double o2) {
                return o1 + o2;
            }
        },
        SUBTRACT(2) {
            @Override
            public double apply (double o1, double o2) {
                return o1 - o2;
            }
        },
        MULTIPLY(3) {
            @Override
            public double apply (double o1, double o2) {
                return o1 * o2;
            }
        },
        DIVIDE(4){
            @Override
            public double apply (double o1, double o2) {
                return o1 / o2;
            }
        };
        public final int precedence;
        Operator(int p) { precedence = p; }
        public abstract double apply (double o1, double o2);
    }
    
    private static Map<String, Operator> ops = new HashMap<String, Operator>() {{
        put("+", Operator.ADD);
        put("-", Operator.SUBTRACT);
        put("*", Operator.MULTIPLY);
        put("/", Operator.DIVIDE);
    }};

    /**
     * The single term of the statement.
     * 
     * Can represent an operator, a parenthesis, or a number.
     */
    private class Token {
        private boolean isOperator = false;
        private boolean isLeftParenthesis = false;
        private boolean isRightParenthesis = false;
        private Operator operator = null;
        private double value;
        public Token(String str) {
            try {
                value = Double.parseDouble(str);
            }
            catch (NumberFormatException e) {
                if (str.equals("(")) {
                    isLeftParenthesis = true;
                } else if (str.equals(")")) {
                    isRightParenthesis = true;
                } else if (ops.containsKey(str)) {
                    operator = ops.get(str);
                    isOperator = true;
                } else {
                    throw e;
                }
            }
        }
        public boolean isOperator () {
            return isOperator;
        }
        public Operator getOperator () {
            // should only be called if the token is the operator
            if (!isOperator)
                throw new NullPointerException("The token is not an operator");
            return operator;
        }
        public boolean isLeftParenthesis () {
            return isLeftParenthesis;
        }
        public boolean isRightParenthesis () {
            return isRightParenthesis;
        }
        public double getValue () {
            // should only be called if the token is the number
            if (isOperator || isRightParenthesis || isLeftParenthesis)
                throw new NullPointerException("The token is not a number");
            return value;
        }
    }
    
    /**
     * Parses a string into an array of token objects.
     * 
     * @param input an input string
     * @return the array of token objects
     */
    private Token[] splitInput(String input) {
        ArrayList<Token> tokens = new ArrayList<>();
        int i = 0;
        while (i < input.length()) {
            char c = input.charAt(i);
            if (c == ' ') {
                i++;
            } else if (ops.containsKey(String.valueOf(c))
                       || c == ')'
                       || c == '(') {
                tokens.add(new Token(String.valueOf(c)));
                i++;
            } else if (Character.isDigit(c) || c == '.') {
                StringBuilder buff = new StringBuilder();
                while (Character.isDigit(c) || c == '.') {
                    buff.append(c);
                    i++;
                    if (i >= input.length())
                        break;
                    c = input.charAt(i);
                }
                try {
                    tokens.add(new Token(buff.toString()));
                } catch (NumberFormatException e) {
                    //nobody cares
                    return null;
                }
            } else {
                //unknown symbol
                return null;
            }
        }
        Token[] output = new Token[tokens.size()];
        return tokens.toArray(output);
    }

    
    private enum TokenType {leftP, rightP, operator, number}
    /**
     * Transforms the infix-notated statement into the postfix-notated.
     * The simple implementation of the Shunting Yard algorithm.
     *
     * @param infix array of token elements representing the infix-notated statement
     * @return array of token elements representing the postfix-notated statement
     */
    private Token[] toPostfix(Token[] infix)
    {
        Deque<Token> postfix = new LinkedList<>();
        Deque<Token> stack  = new LinkedList<>();
        TokenType lastToken = null;
        for (Token token : infix) {
            // operator
            if (token.isOperator()) {
                // special case for +/-, as they can denote the sign, and not the operator
                if ((lastToken == null || lastToken == TokenType.leftP)
                     && (token.getOperator() == Operator.ADD || token.getOperator() == Operator.SUBTRACT)) {
                    postfix.add(new Token("0"));
                // cannot be right after the parenthesis or another operator
                } else if (lastToken == TokenType.leftP || lastToken == TokenType.operator)
                    return null;
                lastToken = TokenType.operator;
                while ( ! stack.isEmpty()
                        && stack.peek().isOperator()
                        && token.getOperator().precedence <= stack.peek().getOperator().precedence)
                    postfix.add(stack.pop());
                stack.push(token);

            // left parenthesis
            } else if (token.isLeftParenthesis ()) {
                // cannot be right after the number or right parenthesis
                if (lastToken == TokenType.number || lastToken == TokenType.rightP)
                    return null;
                lastToken = TokenType.leftP;
                stack.push(token);

            // right parenthesis
            } else if (token.isRightParenthesis ()) {
                // cannot be right after the left parenthesis or the operator
                if (lastToken == TokenType.leftP || lastToken == TokenType.operator)
                    return null;
                lastToken = TokenType.rightP;
                while ( ! stack.peek().isLeftParenthesis ()) {
                    postfix.add(stack.pop());
                    
                    // no matching left parenthesis
                    if (stack.isEmpty())
                        return null;
                }
                stack.pop();
            // number
            } else {
                // cannot be right after the right parenthesis or another number
                if (lastToken == TokenType.rightP || lastToken == TokenType.number)
                    return null;
                lastToken = TokenType.number;
                postfix.add(token);
            }
        }

        while ( ! stack.isEmpty()) {
            // unmatched parentheses
            if (stack.peek().isLeftParenthesis() || stack.peek().isRightParenthesis())
                return null;
            postfix.add(stack.pop());
        }

        Token[] output = new Token[postfix.size()];
        return postfix.toArray(output);
    }
    
    private double calculate(Token[] postfix) {
        Deque<Double> stack = new LinkedList<>();
        for (Token token : postfix) {
            // check for parentheses
            if (token.isLeftParenthesis ()
                || token.isRightParenthesis ())
                throw new IllegalArgumentException("Parentheses are not allowed in postfix notation");
            
            if (token.isOperator()) {
                // enough operands should be on stack
                if (stack.size() < 2)
                    return Double.NaN;
                Operator op = token.getOperator();
                double o1 = stack.pop();
                double o2 = stack.pop();
                double rez = op.apply(o2, o1);
                stack.push(rez);
            } else {
                stack.push(token.getValue());
            }
        }
        // in the end, there can be only one
        if (stack.size() != 1)
            return Double.NaN;
        return stack.pop();
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // basic sanity checks
        if (statement == null || statement.length() == 0)
            return null;
        
        // split the statement into the array of tokens
        Token[] infix = splitInput(statement);
        if (infix == null)
            return null;
        
        // transform the statement into the RPN
        Token[] postfix = toPostfix(infix);
        if (postfix == null)
            return null;
        
        // get the result
        double res = calculate(postfix);
        if (Double.isNaN(res) || Double.isInfinite(res))
            return null;

        // formatting the output
        res = Math.round(res*10000.)/10000.;
        if (res == (long)res)
            return String.valueOf((long)res);
        else
            return String.valueOf(res);
    }

}
