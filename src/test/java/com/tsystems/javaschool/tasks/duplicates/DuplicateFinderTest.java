package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

public class DuplicateFinderTest {

    private DuplicateFinder duplicateFinder = new DuplicateFinder();

    @Test(expected = IllegalArgumentException.class)
    public void test() {
        //run
        duplicateFinder.process(null, new File("a.txt"));

        //assert : exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1() {
        //run
        duplicateFinder.process(new File("a.txt"), null);

        //assert : exception
    }

    @Test
    public void test2() {
        File input = new File("src/test/data/input1");
        File output = new File("src/test/data/output1");
        File reference = new File("src/test/data/ref1");
        //run
        duplicateFinder.process(input, output);
        boolean res = false;
        try {
            res = FileUtils.contentEquals(output, reference);
            output.delete();
        } catch (IOException e) {}
        //assert
        Assert.assertTrue(res);
    }

    @Test
    public void test3() {
        File input = new File("src/test/data/input2");
        File output = new File("src/test/data/output2");
        File reference = new File("src/test/data/ref2");
        File output_head = new File("src/test/data/output2_head");
        try {
            FileUtils.copyFile (output_head, output);
        } catch (IOException e) {}
        //run
        duplicateFinder.process(input, output);
        boolean res = false;
        try {
            res = FileUtils.contentEquals(output, reference);
            output.delete();
        } catch (IOException e) {}
        //assert
        Assert.assertTrue(res);
    }
}
